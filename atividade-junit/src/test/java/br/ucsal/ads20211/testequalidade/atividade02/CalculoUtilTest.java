package br.ucsal.ads20211.testequalidade.atividade02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculoUtilTest {

	@Test
	public void testarFatorial0() {
		int n = 0;
		Long fatorialEsperado = 1L;
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial5() {
		int n = 5;
		Long fatorialEsperado = 120L;
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
